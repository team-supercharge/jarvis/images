# JARVIS Images

This repository contains helper Docker images used as base images in various JARVIS job templates.

* `deploy`: contains everything to execute a deployment on k8s (nodejs, curl, ssh, gnupg, kubectl, helm, aws cli, eksctl, terraform)
* `release`: can perform a standard version release - bumps version, generates changelog, creates git tag (nodejs, git, curl, standard-version)
* `docker-cli`: just docker-cli
* `commitlint`: node image with standard-version preinstalled
* `lockfile-lint`: node image with lockfile-lint preinstalled
* `cover2cover`: Tool to convert jacoco based code coverage reports to cobertura [accepted by gitlab](https://docs.gitlab.com/ee/user/project/merge_requests/test_coverage_visualization.html)

## Deprecated images

* `deploy-capistrano`: builds on top of `deploy`, includes ruby and bundle for capistrano - last version is `1.0.0`
* `deploy-serverless`: builds on top of `deploy`, includes serverless cli - last version is `1.0.0`
* `api-generator`: contains [OASg](https://gitlab.com/team-supercharge/oasg) runtime dependencies - last version is `3.4.0`

# Contributing

Please separate your changes to different images into different commits, and use conventional-commit scopes to correctly label your changes, e.g:

```
feat(deploy): add a new feature
fix(api-generator): fix a bug
```

# Roadmap

* dry-run Docker build for images on merge requests
* migrate built images to DockerHub (?)
