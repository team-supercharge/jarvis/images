# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## [4.1.0](https://gitlab.com/team-supercharge/jarvis/images/compare/v4.0.0...v4.1.0) (2024-03-04)


### Features

* **deploy:** update kubectl to 1.29.2 ([86f1641](https://gitlab.com/team-supercharge/jarvis/images/commit/86f164107a0dd7f9b45049a6aaa811efb625ec96))

## [4.0.0](https://gitlab.com/team-supercharge/jarvis/images/compare/v3.4.0...v4.0.0) (2023-02-22)


### ⚠ BREAKING CHANGES

* deprecate api-generator image in favor of OASg's own base image

### Features

* add Dockerfile for lockfile-lint tool ([aeadb6f](https://gitlab.com/team-supercharge/jarvis/images/commit/aeadb6fcb99a33f87dc12cd26408a1ef78a9342f))
* deprecate api-generator image in favor of OASg's own base image ([e4d73b8](https://gitlab.com/team-supercharge/jarvis/images/commit/e4d73b8fb0fc975fbaa05ff0783dd06fae9a04ff))


### CI

* update to jarvis@8.0.0 ([2e1eb4f](https://gitlab.com/team-supercharge/jarvis/images/commit/2e1eb4f05ad76991e129226d24b56d94b4b72370))

## [3.4.0](https://gitlab.com/team-supercharge/jarvis/images/compare/v3.3.0...v3.4.0) (2022-10-17)


### Features

* **release:** update standard-version to latest 9.5.0 ([98c5812](https://gitlab.com/team-supercharge/jarvis/images/commit/98c58126bcf95aaf165fdf8d54ed2c6b4c51e92d))

## [3.3.0](https://gitlab.com/team-supercharge/jarvis/images/compare/v3.2.0...v3.3.0) (2022-06-01)


### Features

* add cover2cover image ([cf309e6](https://gitlab.com/team-supercharge/jarvis/images/commit/cf309e6af62d9f1e50c2b425b710a253c224c79b))

## [3.2.0](https://gitlab.com/team-supercharge/jarvis/images/compare/v3.1.1...v3.2.0) (2022-03-22)


### Features

* **api-generator:** improve image with python client dependencies ([e06f49b](https://gitlab.com/team-supercharge/jarvis/images/commit/e06f49b26b9445570fc2154ceee966d3540a0456))


### Bug Fixes

* **release:** update to latest versions of alpine packages ([e480cfb](https://gitlab.com/team-supercharge/jarvis/images/commit/e480cfbb539d7aee5c15621451414371016680bf))

### [3.1.1](https://gitlab.com/team-supercharge/jarvis/images/compare/v3.1.0...v3.1.1) (2022-01-13)


### Bug Fixes

* **release:** bump openssh version ([33cac47](https://gitlab.com/team-supercharge/jarvis/images/commit/33cac47b1e58e2e8c501177bd4c80144dfa97c1b))
* **release:** update standard-version ([ac9b90d](https://gitlab.com/team-supercharge/jarvis/images/commit/ac9b90d52c05c35d0035e1467c2969564f414cc4))

## [3.1.0](https://gitlab.com/team-supercharge/jarvis/images/compare/v3.0.0...v3.1.0) (2021-10-01)


### Features

* update images to ubuntu focal ([e0ea92a](https://gitlab.com/team-supercharge/jarvis/images/commit/e0ea92a0303cea324028e13e265fa05261e510e2))


### Bug Fixes

* **release:** update versions of alpine packages ([8d4df02](https://gitlab.com/team-supercharge/jarvis/images/commit/8d4df021eb21a6b6a39d9846dad0bd1b4686773f))

## [3.0.0](https://gitlab.com/team-supercharge/jarvis/images/compare/v2.0.0...v3.0.0) (2021-08-17)


### ⚠ BREAKING CHANGES

* migrate to OSS version hosted on gitlab.com

### Features

* migrate to OSS version hosted on gitlab.com ([0dcb123](https://gitlab.com/team-supercharge/jarvis/images/commit/0dcb12337b6f0bf3df467bbb6da876f640673c1f))


### Bug Fixes

* building release image ([5ef2c13](https://gitlab.com/team-supercharge/jarvis/images/commit/5ef2c13e6e2c8b79f72a81e2c1a1e1ebe500f15c))
* remove global static Git identity config for releases ([ca59b4d](https://gitlab.com/team-supercharge/jarvis/images/commit/ca59b4dc2c5e396a51083debd4602f8b8af95901))
* use versioned image from kaniko executor ([20daf5f](https://gitlab.com/team-supercharge/jarvis/images/commit/20daf5ff3a47ead6e481ac5334da27b9a5b4c1e9))

## [2.0.0](https://gitlab.com/team-supercharge/jarvis/images/compare/v1.0.0...v2.0.0) (2021-03-17)


### ⚠ BREAKING CHANGES

* **deploy-serverless:** image deprecated and removed from source
* **deploy-capistrano:** image deprecated and removed from source

### Features

* **deploy-capistrano:** image deprecated and removed from source ([ed5b06d](https://gitlab.com/team-supercharge/jarvis/images/commit/ed5b06dd4b6d1e829fd73f99add1abf62986978b))
* **deploy-serverless:** image deprecated and removed from source ([3325374](https://gitlab.com/team-supercharge/jarvis/images/commit/33253748b7674ef250bd6dfdd98751afe9034568))
* **release:** change base image to use gitlab-org/release-cli ([955a400](https://gitlab.com/team-supercharge/jarvis/images/commit/955a40048f156439118632567a7a0922360863ca))

## 1.0.0 (2021-03-17)


### Features

* initialize project with README ([2932428](https://gitlab.com/team-supercharge/jarvis/images/commit/2932428a7562678549bf882725a9acb282b7c41c))
* make valid Dockerfile in terms of linting for every image ([71243b0](https://gitlab.com/team-supercharge/jarvis/images/commit/71243b03b114cbdc4fd1fb02fa7bcbebf9e6191d))
* migrate images from main Jarvis repository and setup CI ([2b0a599](https://gitlab.com/team-supercharge/jarvis/images/commit/2b0a5993b14419f96f920c5bcaa73850fdb22f20))


### Bug Fixes

* **deploy-capistrano:** use correct ruby version on install ([240e214](https://gitlab.com/team-supercharge/jarvis/images/commit/240e214fe3e0f26ef3bf94783b610a3d36e7f62c))
