mkdir -p ~/.ssh
chmod 700 ~/.ssh
touch ~/.ssh/known_hosts
chmod 644 ~/.ssh/known_hosts
apt update && apt upgrade -y && apt install curl software-properties-common unzip -y
curl -sL https://deb.nodesource.com/setup_12.x | bash -
apt update && apt install nodejs git git-lfs unzip curl openssh-client software-properties-common apt-transport-https gnupg2 -y
git config --global user.email "gitlab-ci@scapps.io"
git config --global user.name "GitLab CI"
ssh-keyscan -t rsa gitlab.supercharge.io >> ~/.ssh/known_hosts
curl -LO "https://dl.k8s.io/release/v1.29.2/bin/linux/amd64/kubectl"
chmod +x kubectl
mv kubectl /usr/local/bin
curl -o helm.tar.gz https://get.helm.sh/helm-v3.7.2-linux-amd64.tar.gz
tar -zxvf helm.tar.gz
mv linux-amd64/helm /usr/local/bin/helm
curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"
unzip awscliv2.zip
./aws/install
curl --silent --location "https://github.com/weaveworks/eksctl/releases/latest/download/eksctl_$(uname -s)_amd64.tar.gz" | tar xz -C /tmp
mv /tmp/eksctl /usr/local/bin
curl --silent -o /tmp/tf.zip https://releases.hashicorp.com/terraform/0.13.1/terraform_0.13.1_linux_amd64.zip && unzip /tmp/tf.zip -d /usr/local/bin/
